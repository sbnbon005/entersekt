package com.bongani;

public class Directory {
    private String path;
    private Attribute attr;

    public Directory(){

    }

    public Directory(String path, Attribute attr){
        this.path = path;
        this.attr = attr;
    }

    public String getPath() {
        return this.path;
    }

    public Attribute getAttr() {
        return this.attr;
    }
}
